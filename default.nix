{ pkgs ? import <nixpkgs> {} }:

rec {
  lutris = pkgs.callPackages ./pkgs/lutris.nix {};
}
